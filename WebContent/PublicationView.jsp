<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Publication View</title>
</head>
<%@ include file="Header.jsp" %>
<body>

	<table>
			<tr>
			<th>Id</th><th>Title</th><th>Publication Name</th><th>Publication Date</th><th>Authors</th><th>Cite Count</th>
			</tr>
		
		     <tr>
		     <td> ${publication.id}</td>
		     
		     
		     <td> ${publication.title}</td>
		     
		     
		     <td> ${publication.publicationName}</td>
		     
		     
		     <td> ${publication.publicationDate}</td>
		     
		     
		     <td> ${publication.authors}</td>
		     
		     
		     <td> ${publication.citeCount}</td>
		     </tr>
		
	</table>

</body>
</html>