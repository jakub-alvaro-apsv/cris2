<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin View</title>
</head>
<body>
<%@ include file="Header.jsp" %>

<form action="CreateResearcherServlet" method="post">
        <input type="text" name="id" placeholder="User Id">
        <input type="text" name="name" placeholder="Name">
        <input type="text" name="lastname" placeholder="Last name">
        <input type="text" name="email" placeholder="Email">
        <input type="text" name="password" placeholder="Password">
        <input type="text" name="scopusURL" placeholder="Scopus URL">
        <button type="submit">Create Researcher</button>
</form>

<form action="CreatePublicationServlet" method="post">
        <input type="text" name="id" placeholder="Publication Id">
        <input type="text" name="title" placeholder="Title">
        <input type="text" name="publicationName" placeholder="Publication Name">
        <input type="date" name="publicationDate" placeholder="Publication Date">
        <input type="text" name="authors" placeholder="Authors">
        <button type="submit">Create Publication</button>
</form>

</body>
</html>