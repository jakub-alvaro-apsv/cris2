package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;


@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Researcher user = (Researcher) request.getSession().getAttribute("user"); //para comprobar que usuario esta registrado
		String userId = user.getId();
		
		if (userId.equals("root")) {//compruebo si es admin
	        
			String id = request.getParameter("id");
			String title = request.getParameter("title");
			String publicationName = request.getParameter("publicationName");
			String publicationDate = request.getParameter("publicationDate");
			String authors = request.getParameter("athors");
			
			Publication p = new Publication();
			
			p.setId(id);
			p.setTitle(title);
			p.setPublicationName(publicationName);
			p.setPublicationDate(publicationDate);
			p.setAuthors(authors);
			p.setCiteCount(0);//por defecto al crear publicacion tiene citecount 0
		
			
			try {//llamada al cliente REST para crear la publication
				
				Client client = ClientBuilder.newClient(new ClientConfig());
				
				client.target("http://localhost:8080/CRISSERVICE/rest/Publications/")//URL a la que ataco
						.request()
						.post(Entity.entity(p, MediaType.APPLICATION_JSON), Response.class);//en el cuerpo de la peticion va el objeto Publication p       
				
				response.sendRedirect(request.getContextPath() + "/AdminServlet");
			} catch (Exception e) {
			 //captura el error que se produce si se intenta crear un reseacher con un id ya existente
			System.out.println("Algo ha ido mal al introducirlo en la BBDD");
			}
		}else {//si no estas autenticado como admin te lleva a LoginView
			getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request,response);
		}
		
	}

}
