package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;


@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Researcher user = (Researcher) request.getSession().getAttribute("user");
		String userId = user.getId();
		
		if (userId.equals("root")) {//compruebo si es admin
	        
			String id = request.getParameter("id");
			String name = request.getParameter("name");
			String lastname = request.getParameter("lastname");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String scopusURL = request.getParameter("scopusURL");
			
			Researcher r = new Researcher();
			
			r.setId(id);
			r.setName(name);
			r.setLastname(lastname);
			r.setEmail(email);
			r.setPassword(password);
			r.setScopusURL(scopusURL);
		
			
			try {//llamada al cliente REST para crear al researcher
				
				Client client = ClientBuilder.newClient(new ClientConfig());
				
				client.target("http://localhost:8080/CRISSERVICE/rest/Researchers/")//URL a la que ataco
						.request()
						.post(Entity.entity(r, MediaType.APPLICATION_JSON), Response.class);//en el cuerpo de la peticion va el objeto Researcher       
				
				response.sendRedirect(request.getContextPath() + "/AdminServlet");
			} catch (Exception e) {
			 //captura el error que se produce si se intenta crear un reseacher con un id ya existente
			}
		}else {//si no estas autenticado como admin te lleva a LoginView
			getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request,response);
		}
		
	}

}
